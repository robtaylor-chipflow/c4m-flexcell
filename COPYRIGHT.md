The git version control software is used to track authorship and copyright of all the contributions to this repository.
The information can thus be retrieved by cloning [the upstream repository](https://gitlab.com/Chips4Makers/c4m-flexcell).

Distributions of source code that do not retain the git repository information, need to make sure they fulfill the retainment of copyright and other information as requested by the [applicable source code distribution licenses](LICENSE.md).
